set -U fish_greeting

set -x XKB_DEFAULT_LAYOUT "pl"

# ENV
set -x EDITOR "/usr/bin/nvim"
set -x GOPATH $HOME/.go
set -x ALSA_CARD Generic
set -x GPG_TTY (tty)

fish_add_path $HOME/.cargo/bin

# ALIASES
alias vim="nvim"
