function fish_prompt
    # [alfa@nobby] /path/to/dir $
    and set -l ret green; or set -l ret red
    set -l symbol '$'
    set -l color $fish_color_cwd
    printf "[%s%s%s@%s%s] %s%s%s%s %s%s%s%s " \
    (set_color $color)\
    $USER\
    (set_color normal)\
    $hostname\
    (set_color normal)\
    (set_color blue --bold)\
    (prompt_pwd)\
    (set_color normal)\
    (fish_git_prompt)\
    (set_color blue)\
    (set_color $ret)\
    $symbol\
    (set_color normal)
end
