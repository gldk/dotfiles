# Linux Void base iso setup instructions
* uefi boot
* amd gpu and cpu
* wayland display server protocol
* my software of use


## Index

1. [Installer](#installer)
1. [Startup](#startup)
1. [Firmware](#firmware)
1. [Graphics](#graphics)
1. [Editor](#editor)
1. [Shell](#shell)
1. [Wayland](#wayland)
   1. [Seat management](#seat-management)
   1. [Compositor](#compositor)
1. [Terminal](#terminal)
1. [Sound](#sound)


## Installer

### UEFI

* Set GPT partition scheme
* Set 256M EFI System vfat fs at `/boot/efi`
* NVMe ext4 at `/`
* HDD ext4 at `/home`


## Startup

* `xbps-install -Su xbps`
* `xbps-install -Su`
* `xbps-install git`
* `git clone --recurse-submodules https://codeberg.org/gldk/dotfiles.git`
* `mkdir ~/.config && cp -r dotfiles/. ~/.config/`


## Firmware

* `xbps-install linux-firmware-amd`


## Graphics

* `xbps-install mesa-dri`
* `xbps-install vulkan-loader mesa-vulkan-radeon amdvlk`
* `xbps-install amdgpu`
* `xbps-install mesa-vaapi mesa-vdpau`


## Editor

* `xbps-install neovim`
* `xbps-install gcc` install any C compiler to compile language servers
* run `nvim` and wait for packer to update setup


## Shell

* `xbps-install fish-shell`
* `chsh -s /url/bin/fish`


## Wayland

* `xbps-install wayland wlroots`


### Seat management

* `xbps-install dbus seatd`
* add user to seatd group `usermod -aG _seatd <user>`
* enable dbus `ln -s /etc/sv/dbus /var/service/`
* enable seatd `ln -s /etc/sv/seatd /var/service/`
* get rid of XDG_RUNTIME_DIR problem
  * `xbps-install dumb_runtime_dir`
  * add line `session optional pam_dumb_runtime_dir.so` to `/etc/pam.d/system-login`
* reboot


### Compositor

* `xbps-install river`
* `xbps-install fuzzel` app launcher for wayland
* `xbps-install Waybar` status bar


## Terminal

* `xbps-install foot`
* `xbps-install nerd-fonts` any monospace font would do


## Sound

* `xbps-install apulse alsa-utils`
* setup alsamixer
  * list cards with `cat /proc/asound/cards`
  * find which one is your prefered one and their drivers
    * `HDA-Intel` driver is called `snd-hda-intel`
  * to fix card positions edit `/etc/modprobe.d/alsa-base.conf`
    * add a line like`options snd slots=,snd-hda-intel`
  * select prefered card in `/etc/asound.conf`
    * `defaults.pcm.card <n>\ndefaults.ctl.card <n>`; `<n>` is card index
